# Mastodon Desktop Client Design and Requirements

## Preamble

The following describes the functionality desired for the first release of a desktop Mastodon client for use with multiple accounts. Other behaviors, features, and customizability may be desired later.
In order to obtain the best long term product viability, this specification avoids unnecessary implementation details such as programming language or other tooling requirements in favor of promoting the personal autonomy of those providing labor. Algorithms and any other implementation suggestions present are provided to indicate the desirable aspects of their processing models and should be considered negotiable.

Respect for labor and human dignity are important to this project. Funders reserve the right to prioritize worker-owned entities and representatives of marginalized groups in contracting and other compensation.

The project is sponsored for community benefit and personal use. While there is no profit motive, neither has any effort been made to establish nonprofit governance. In the event that participation should include 5 or more active individuals at some point, the Contributor Covenant will be in effect until a more specific agreement can be reached on a more effective way to grow the community safely.

This specification forms the basis for a Request For Proposals that will follow.

## Target Platforms

 - Universal Windows Platform (UWP)
 - In browser
 - Android
 - Linux and BSD
 - iOS, MacOS, and others as targets of opportunity

## Licensing

 - All application code will be available for use under AGPL 3.0+
 - All documentation will be available under GFDL, version to be specified
 - Contributors should include a statement that they are the authors of the code submitted or describe the nature of the license on which the contribution is submitted in order for their contributions to be accept in the main repository as fulfillment of obligations incurred under the license. Contributors not wishing to make such a statement may fulfill the requirements of the license terms by maintaining their own repository.
 - Contributors will also have the opportunity to indicate the terms under which they'd be willing to relicense their contributions. This is a survey, not a grant of permission to relicense. By documenting contributor intent, we may be able to better server the community as requirements and options change.

## Technology recommendations

The criteria for tooling choice are support for deploying to the target platforms, community opinion of the final product, and suitability for small team development.

At least 3 tool chains support cross platform development and could provide the requested feature set in a reasonable timeframe from a single developer or small group.
 - Ionic Platform uses Node and Angular with HTML/CSS for developer facing tools. The primary drawback is that desktop applications are built with Electron, which has a poor reputation for performance and privacy on the fediverse.
 - Qt is natively a C++ library, which also allows it to be used from other programming languages provided the developer can find or build bindings. Licensing for Qt would not require a fee for GPL-style FOSS release, but would prevent release under more permissive licenses.
 - Godot Engine is a 2D gaming engine with a permissive license. It has its own scripting language as well as support for several other languages, including C++. It's a platform, not a library, which makes it easier to use for supported languages and harder to use from languages that are not already supported or that have only partial support. 
 - Other multiplatform alternatives, like Mozilla's XUL Runner, might be feasible for a developer who has experience and personal tooling they are willing to share with the community.

## Data store

The application needs to store the following items for random access:
 - Posts
 - Profiles
 - Instance information
 - Media

Metadata should be stored indicating the sources through which the client has received posts.

Media may be expired automatically to free up storage space.

Feeds are implemented as indexes into the central store of posts. Receiving posts with hashtags should create or update a corresponding feed in the background for each hashtag.

Indexes should also be maintained for follower and following connections.

Since this application is not designed for a server environment, the database or other data store should be embedded rather than dependent on installation of a separate service.

## Start up

The application should open with the same feeds active as when the application was closed.

Active feeds should be refreshed in the order displayed. The application should display a visual indication of progress and remain as responsive as possible during the process. 

Follower,  following, and inactive feeds may be refreshed after active feeds in any logical order.

## Register with servers

The OAuth2 device workflow is used to connect the application to existing accounts on servers implementing the Mastodon Client API. Implementation details may vary depending on the facilities offered by the developer's toolkit. Developer should follow the security recommendations of the specification.

## API functions

API functions listed below are adequately described in the Mastodon API documentation and directly linked to functionality expected of the client. Developer(s) need to implement the API calls, persist results to a data store and bind the data retrieved to views, some of which may be populated with data from more than one feed source.

### Account handling
 - Fetch account information
 - Get current user
 - Update current user
 - Get those who follow user
 - Get those who the user follows
 - Get status posted on the account
 - Follow an account
 - Unfollow an account
 - Block an account
 - Unblock an account
 - Mute an account
 - Unmute an account
 - Search for an account

### Blocks
 - Get list of blocked users
 - Get list of blocked domains
 - Block a domain
 - Unblock a domain
 - Fetch favorites

### Filters
 - Fetch filter list
 - Create a filter
 - Get details of a filter
 - Update a filter
 - Delete a filter

### Follow requests
 - Fetch list of follow requests
 - Authorize follow request
 - Reject follow request

### Follow suggestions
 - Get list of follow suggestions
 - Delete user from list of suggestions

### Following
 - Follow a user

### Instances
 - Get instance information
 - Get custom emojis

### Lists
 - Retrieve lists
 - Retrieve lists by membership
 - Retrieve accounts in a list
 - Retrieve a list
 - Create a list
 - Update a list
 - Delete a list
 - Add accounts to list
 - Remove accounts from list

### Media
 - Upload a media attachment

### Mutes
 - Fetch a user's mutes

### Notifications
 - Fetch a user's notifications
 - Get a notification by id
 - Clearing a user's notifications
 - Dismissing a single notification
 - Adding a push subscription
 - Get current push subscription status
 - Updating a push subscription
 - Removing a push subscription

### Reports
 - Fetching a user's reports
 - Reporting a user

### Search
 - Searching for content

### Statuses
 - Fetching a status
 - Getting context of a status
 - Getting the card associated with a status
 - Get who reblogged a status
 - Get who favorited a status
 - Post a status
 - Delete a status
 - Reblog a status
 - Undo a reblog
 - Favoriting
 - Unfavorite
 - Pinning
 - Unpin
 - Muting
 - Unmute

### Timelines
 - Retrieve home timeline
 - Retrieve public timeline
 - Retrieve tag timeline
 - Retrieve direct message timeline

### The following features of the Mastodon API have been deliberately omitted from the requirements:
 - Pin/unpin an account
 - Get relationships
 - Update a media attachment
 - Endorsements


### Requirements not directly derived from capabilities provided by the API:

The application must support simultaneous display of feeds from multiple accounts. This implies that some means other than quarantining posts to a tab or window associated with one account is necessary to inform interactions with content.

Create custom feeds from all received statuses (to track toots about specific interests). The simplest algorithm for this is probably merge and filter. The ability to select source feeds is not a requirement at this time.

Create a system that will use metadata as well as status contents to supress display of received messages (leverage the CW system and other data to filter out noise) based on presence of unwanted terms. Filters created on Mastodon servers affect all retrieved messages on one account and only match against the message body. This requirement is for filters that can be applied locally on a per-feed basis and that include post metadata like subject (cw) and display name of the poster by default.

Improve support for hashtags over server-side processing by indexing tags in incoming messages, populating a feed from local storage when a hashtag is first clicked, and merging queries from available sources.

## Visuals

The application should provide the user with system borders and controls consistent with prevailing expectations on that platform. The app should not conceal the address bar in the browser, the notification area in mobile devices, or system areas in desktop environments without user action.

The application should provide an n-column display. 

On mobile devices, on low resolution displays, or when the size of the application window indicates portrait mode, the app should default to a single column with one active feed, a visual indication of the feed contents, and a menu icon to open the control panel as an overlay. 

When the display area supports multiple columns, the default number of columns should be appropriate for the aspect ratio of the window. When there are 2 or more columns, the control panel appears in the first column based on the direction in which the locale language is read. (That's the left column for English and the right column for RTL languages such as Arabic.) 

The application may also provide tabs, tiling, or other UI patterns native to the toolkit chosen for the final project. A developer proposing a different UI pattern instead of, rather than in addition to, a column layout must make this unambiguous in their bid and receive explicit acknowledgement of this deviation prior to accepting payment and commencing to perform the work.

Some UI items should be expressed as columns or equivalent entities, meaning that they contain sufficient information to justify using a whole column to display:

 - OAuth2 workflow (signing in to accounts)
 - View profile
 - Edit profile
 - View list of accounts
 - List of domains
 - View list of filters
 - Display instance information
 - View lists
 - View a list
 - Create or edit a list
 - View a feed (list of statuses or notifications)
 - View a list of reports
 -  Upload media dialog
 -  Control panel for administering accounts, filters, and feeds
 -  Create or edit local feed
 -  Create or edit local filter

Some information may be represented compactly as cards or a similar construct. Cards that expand to display more information or additional controls when they have focus are also listed above. This is a list of items that have abbreviated views:
 - View profile
 - View filter
 - Display instance information
 - View a list
 - View an individual status or notification
 - View a report
 - Upload media dialog
 - Create or edit local feed
 - Create or edit local filter

Generally speaking, any list, collection, or other group of items will be a column. Any list item or member of a collection will have a card format. For any item that has a card format but also appears as a column entity, the column entity represents a full view while the card represents an abbreviated view. When create and edit activities are to be made available as both cards and columns, the card item represents a simple way to perform the interaction while the column view provides advanced options and additional controls.

The following may be presented as modals, or popups:
 
 - Display instance information
 - Display instance emoji in pick list
 - Control panel for administering accounts, filters, and feeds (in single column view)
 - Confirmation request for actions that cannot be undone
 - Display media
 - Display confirmation of actions performed that may not be visible
 - Notifications of new activity

It's often desirable that modals not requiring interactions dismiss themselves when this is supported. This is called a toast.


## Behavior of edit and create actions

Since we are designing primarily a desktop client for people who access multiple accounts concurrently, people using the software should not be limited to one compose action at a time as is the case for the Mastodon web UI. Instead, they should be able to have one create or edit operation available in each content column. When a second activity is created in a column or when an activity currently displayed as a card scrolls out of view, the inactive activity is moved to the control panel where it is represented by a card at the top of the list of open activities where the user can either interact with it in card form or expand to a column view.

When an activity has been opened in column mode and settings have been edited that are not visible in the card view, the added information is retained and the icon to expand the card to column view should change appearance to indicate the existence of information not currently displayed.

Iconography will be provided through Fork Awesome. Implementers should plan to export SVG to bitmaps in the resolutions required. Contribution to the upstream project of a globe icon that features North America less prominently would be supported.

## Summary

Over 2 million accounts have been created on Mastodon instances. The social mix of people interacting on the service makes the community literate in our demand for software with high standards socially as well as technologically. We want free software and we want to pay for it. We believe that the lack of diversity in tech fields is a result of social biases that require correction. For these reasons, this specification is as much about expressing the values of the community in the creation process as it is about creating a product that would be useful for the community.

